<?php
/*
Template Name: Страница Наш блог
*/
get_header('page');
?>
    <div class="w-100" style="background: url(<? echo get_the_post_thumbnail_url() ?>) no-repeat; background-size:cover; height: 36rem">
        <div class="container h-100 w-100 d-flex align-items-center"  >
            <div class="row">
                <div class="col">
                    <h1 class="text-white font-size-6_5  font-weight-bold-title UniSans-Heavy">НАШ БЛОГ</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <?php query_posts('cat=8'); ?>

        <?php if (have_posts()) : ?>

            <?php while (have_posts()) : the_post(); ?>
                <div class="row py-4">
                    <div class="col">
                        <a href="<? the_permalink(); ?>" class="blog-title-color">
                            <span class="font-size-36 font-weight-very-bold UniSans-Heavy">
                                <?php the_title(); ?>
                            </span>
                        </a>
                        <span>
                            <?php the_excerpt(); ?>
                        </span>
                        <div class="d-flex justify-content-start">
                            <a href="<? the_permalink(); ?>" class="text-white btn button-red-style">
                                Читать далее...
                            </a>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>

        <?php else : ?>

            <h2  class=" UniSans-Heavy">Записей нет</h2>

        <?php endif; ?>
    </div>
<?php
get_footer('page');
?>