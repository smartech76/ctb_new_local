<?php
/*
Template Name: Главная страница
*/
get_header(); ?>
    <body>
    <div class="w-100 banner-paralax">
        <div class="d-md-none h-100 d-flex align-items-end">
            <script type="text/javascript" src="//ui.sletat.ru/module-5.0/app.js" charset="utf-8"></script>
            <script type="text/javascript">sletat.createModule5('Search', {
                    files: ["//ui.sletat.ru/module-5.0/theme/sea_dec2015.min.css"],
                    files: ["<?php bloginfo("template_directory");?>/css/fraim.css"],
                    enabledCurrencies: ["RUB", "EUR", "USD"],
                    useCard: false,
                });</script>
            <span class="sletat-copyright">Идет загрузка модуля <a href="//sletat.ru/" title="поиск туров"
                                                                   target="_blank">поиска туров</a> &hellip;</span>
        </div>
        <div id="target" class="parallax-port d-none d-md-block">
            <img class="parallax-layer" src="<?php bloginfo("template_directory"); ?>/images/target_blue4.png"
                 alt="ПОИСК ТУРА"/>
            <img class="parallax-layer paralax-text img-fluidg"
                 src="<?php bloginfo("template_directory"); ?>/images/target_green4.png" alt="ПОИСК ТУРА"/>
            <img class="parallax-layer" src="<?php bloginfo("template_directory"); ?>/images/target_red4.png"
                 alt="ПОИСК ТУРА"/>
            <div class="d-flex flex-column align-items-center justify-content-end h-100 position-absolute w-100"
                 id="top-1">
                <div class="d-none d-lg-flex w-75 justify-content-around">
                    <div>
                        <a href="<?php echo get_permalink(92); ?>" class="text-white-no_impotant text-hover-gold">
                            <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/ico-1.png"
                                 alt="">
                            <span class="font-size-1_4">Горячие</span>
                        </a>
                    </div>
                    <div>
                        <a href="<?php echo get_permalink(95); ?>" class="text-white-no_impotant text-hover-gold">
                            <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/ico-2.png"
                                 alt="">
                            <span class="font-size-1_4">Раннее Бронирование</span>
                        </a>
                    </div>
                    <div>
                        <a href="<?php echo get_permalink(294); ?>" class="text-white-no_impotant text-hover-gold">
                            <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/ico-3.png"
                                 alt="">
                            <span class="font-size-1_4">Заказать трансфер</span>
                        </a>
                    </div>
                </div>
                <div class="w-75 mb-4">
                    <script type="text/javascript" src="//ui.sletat.ru/module-5.0/app.js" charset="utf-8"></script>
                    <script type="text/javascript">sletat.createModule5('Search', {
                            files: ["//ui.sletat.ru/module-5.0/theme/sea_dec2015.min.css"],
                            files: ["<?php bloginfo("template_directory");?>/css/fraim.css"],
                            enabledCurrencies: ["RUB", "EUR", "USD"],
                            useCard: false,
                        });</script>
                    <span class="sletat-copyright">Идет загрузка модуля поиска туров</span>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 d-none d-md-block">
        <div class="first-carousel mb-minus-6">
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-1.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-2.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-3.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-4.png" alt="">
            </div>
        </div>
    </div>
    <div class="w-100 d-md-none">
        <div class="first-carousel-small">
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-1-small.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-2-small.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-3-small.png" alt="">
            </div>
            <div>
                <img class="img-fluid" src="<?php bloginfo("template_directory"); ?>/images/slide-4-small.png" alt="">
            </div>
        </div>
    </div>
    <div class="w-100 front-page-background d-custom-md-none">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-column align-items-end justify-content-center h-47r">
                    <div>
                        <span class="font-weight-very-bold text-white line-height-1 font-size-4_7 UniSans-Heavy">ЗАКАЖИ <br> ПОДБОР <br> ТУРА</span>
                        <div class="mt-3">
                            <button class="text-white eModal-18  btn button-red-style py-3 px-5 font-weight-normal font-size-18">
                                ЗАКАЗАТЬ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 front-page-background-md d-custom-md-visible">
        <div class="container">
            <div class="row">
                <div class="col d-flex justify-content-center  mt-3">
                    <div class="d-flex flex-column align-items-center text-center">
                        <span class="font-weight-very-bold text-white line-height-1 font-size-4_7 UniSans-Heavy">ЗАКАЖИ ПОДБОР ТУРА</span>
                        <div>
                            <button class="mt-3 mt-sm-5 eModal-18  text-white btn button-red-style py-3 px-5 font-weight-normal">
                                ЗАКАЗАТЬ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col text-center my-3">
                <h2 class="text-brown font-weight-very-bold font-size-50 UniSans-Heavy">ПАРТНЁРЫ</h2>
            </div>
        </div>
        <div class="row mb-0 mb-lg-0 ">
            <div class="col mb-3 d-flex align-items-center justify-content-center flex-md-column">
                <div class="d-flex d-md-block flex-column">
                    <img class="img-fluid"
                         src="<?php bloginfo("template_directory"); ?>/images/logo-partners/partners-anex.png" alt="">
                    <img class="img-fluid"
                         src="<?php bloginfo("template_directory"); ?>/images/logo-partners/partners-biblio.png" alt="">
                    <img class="img-fluid"
                         src="<?php bloginfo("template_directory"); ?>/images/logo-partners/partners-pegas.png" alt="">
                    <img class="img-fluid"
                         src="<?php bloginfo("template_directory"); ?>/images/logo-partners/Alean.png"
                         alt="">
                    <img class="img-fluid d-none d-lg-inline"
                         src="<?php bloginfo("template_directory"); ?>/images/logo-partners/CTB_sait_logo_VIking.png"
                         alt="">
                </div>
            </div>
        </div>
        <div class="row pt-0 pt-md-5 pb-4">
            <div class="col text-center">
                <a href="<?php echo get_permalink(115); ?>"
                   class="text-white btn button-red-style font-size-18 px-5">
                    ВСЕ ПАРТНЁРЫ
                </a>
            </div>
        </div>
    </div>
    <div class="w-100 black-background-block ">
        <div class="container">
            <div class="row pt-5 pb-2">
                <div class="col text-center">
                    <h3 class="text-white font-weight-very-bold font-size-50 UniSans-Heavy">НАША КОМАНДА</h3>
                </div>
            </div>
            <div class="row pt-3 d-none d-md-block">
                <div class="col d-flex">
                    <div class="thumbnail">
                       <img src="<?php bloginfo("template_directory"); ?>/images/team/Popova-logo.jpg" class="eModal-23" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3 font-weight-very-bold font-size-24">Юлия Попова</span>
                            <span>Генеральный директор</span>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/images/team/Morozova-logo.jpg" class="eModal-24" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3 font-weight-very-bold font-size-24">Ольга Морозова</span>
                            <span>Директор по развитию</span>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/images/team/Yakovleva-logo.jpg" class="eModal-25" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3 font-weight-very-bold font-size-24">Анна Яковлева</span>
                            <span class="font-size-1-md">Менеджер по выездному туризму и индивидуальным турам</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-none d-md-block">
                <div class="col d-flex justify-content-center">
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/images/team/Gribanova-logo.jpg" class="eModal-26" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3 font-weight-very-bold font-size-24">Любовь Грибанова</span>
                            <span>Менеджер по выездному туризму и групповым турам</span>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <img src="<?php bloginfo("template_directory"); ?>/images/team/Musinova-logo.jpg" class="eModal-27" alt="">
                        <div class="caption d-flex flex-column">
                            <span class="pt-4 pb-3 font-weight-very-bold font-size-24">Ирина Мусинова</span>
                            <span>Менеджер по внутреннему и выездному туризму</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row d-md-none">
                <div class="col">
                    <div class="second-carousel">
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory"); ?>/images/team/Popova-logo.jpg" class="eModal-23" alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Юлия Попова</span>
                                    <span>Генеральный директор</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory"); ?>/images/team/Morozova-logo.jpg" class="eModal-24"
                                     alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Ольга Морозова</span>
                                    <span>Директор по развитию</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory"); ?>/images/team/Yakovleva-logo.jpg" class="eModal-25"
                                     alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Анна Яковлева</span>
                                    <span>Менеджер по выездному туризму и индивидуальным турам</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory"); ?>/images/team/Gribanova-logo.jpg" class="eModal-26"
                                     alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Любовь Грибанова</span>
                                    <span>Менеджер по выездному туризму и групповым турам</span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="thumbnail">
                                <img src="<?php bloginfo("template_directory"); ?>/images/team/Musinova-logo.jpg" class="eModal-27"
                                     alt="">
                                <div class="caption d-flex flex-column">
                                    <span class="pt-4 pb-3">Ирина Мусинова</span>
                                    <span>Менеджер по внутреннему и выездному туризму</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100">
        <div class="container">
            <div class="row pt-5 pb-2">
                <div class="col text-center">
                    <h3 class="text-brown font-weight-very-bold font-size-50 UniSans-Heavy">ОТЗЫВЫ ТУРИСТОВ</h3>
                </div>
            </div>
            <div class="row my-5">
                <div class="col third-carousel">
                    <?php 
                    global $wp_query;
                    $save_wpq = $wp_query;
                    $args = array(
                        'cat' => 4,
                        'posts_per_page' => 4
                    );
                    $wp_query = new WP_Query( $args );
                    ?>
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                    <div class="d-flex flex-column justify-content-center flex-md-row">                      
                        <div class="col-12 col-md-5 ">
                            <div class="d-flex flex-column align-items-center">
                                <div>
                                    <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-1 my-3 my-md-0">
                            <div class="d-flex justify-content-center">
                                <img src="<?php bloginfo("template_directory"); ?>/images/quates.png" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="d-flex flex-column">
                                <span class="">
                                    <?php echo(get_post_meta($post->ID, 'date', true)); ?>
                                    <br/>
                                    <?php the_excerpt(); ?>
                                </span>
                                <span class="mt-5 font-weight-bold text-right"><?php echo(get_post_meta($post->ID, 'name', true)); ?></span>
                            </div>
                        </div>
                    </div>                    
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
                <div class="modal-for-third-carousel">
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="custom-modal-wrapper">
                            <div class="modal fade" id="exampleModal<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="font-weight-bold"><?php the_content(); ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                  
                        <?php endwhile; ?>
                        <?php endif; ?>
                    <?php $wp_query = $save_wpq; ?>                      
                </div>                   
            </div>
            <div class="row">
                <div class="col d-flex justify-content-center mb-4">
                    <a href="<?php echo get_permalink(100); ?>"
                       class="text-white btn button-red-style font-size-18 d-none d-lg-block">
                        ВСЕ ОТЗЫВЫ
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();
