<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ctb
 */
get_header('page');
?>
    <div class="w-100 banner-hot-background">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title-banner-block d-flex justify-content-center flex-column">
                        <h1 class="text-white font-weight-very-bold font-size-5 UniSans-Heavy font-size-5-md">УПС!<br/>У НАС ТАКОЙ СТРАНИЦЫ НЕТ</h1>
                        <a href="https://ctb76.ru" class="text-white-no_impotant text-hover-gold font-size-1_4">Вернуться на главную</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
				
<?php get_footer(); ?>
