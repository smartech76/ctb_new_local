jQuery(document).ready(function(){

	function initFancybox() {

		var allGallaryLinks = jQuery('[open-fancybox-gallery]');

		allGallaryLinks.each(function(index, element) {

			var $link = jQuery(element);
			var gallaryID = $link.attr("open-fancybox-gallery");
			var $targetLink = jQuery('[target-fancybox-gallery="' + gallaryID + '"]');
			var gallary = jQuery('[data-fancybox="gallery-' + gallaryID + '"]');

			$link.click(function () {
				$targetLink.click();
			});

			gallary.fancybox({
				loop: true,
				toolbar: true,
				buttons: [
				    "zoom",
				    "slideShow",
				    "fullScreen",
				    "thumbs",
					"close"
				]
			});

		});
	}

	initFancybox();

});