// Paralax script
jQuery(document).ready(function(){
    function initTargetParalax () {     
        var target = jQuery("#target");
        target
            .children('img')
            .parallax(
                { mouseport: target },
                { xparallax: 0.2, yparallax: 0.2 },     // Blue layer options
                { xparallax: 0.6, yparallax: 0.6 },     // Green layer options
                { }                                      // Red layer options
            );
    } 
    setTimeout(initTargetParalax, 4000);
});

  