jQuery(document).ready(function () {
    window.onscroll = function () {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        document.getElementById('menu').style.backgroundColor = scrolled == 0 ? "#ffffff00" : "black";
    };
});