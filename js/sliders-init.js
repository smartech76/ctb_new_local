jQuery(document).ready(function () {

    jQuery('.first-carousel').slick({
        prevArrow: '<div class="slick-prev" aria-label="Previous"  style="">' +
        '<img class="img-fluid" src="https://ctb76.ru/wp-content/themes/ctb_new_local/images/arro-left.png" alt="">' +
        '</div>',
        nextArrow: '<div class="slick-next" aria-label="Next"  style="">' +
        '<img class="img-fluid" src="https://ctb76.ru/wp-content/themes/ctb_new_local/images/arro-right.png" alt="">' +
        '</div>'
    });

    jQuery('.first-carousel-small').slick({
        dots: true,
        arrows: false
    });

    jQuery('.second-carousel').slick({
        dots: true,
        arrows: false
    });

    jQuery('.third-carousel').slick({
        dots: true,
        arrows: true,
        prevArrow: '<button class="slick-prev slick-arrow d-none d-md-block" aria-label="Previous" type="button" style="">Previous</button>',
        nextArrow: '<button class="slick-next slick-arrow d-none d-md-block" aria-label="Next" type="button" style="">Next</button>'
    });

    jQuery('.post-slider').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,

    });

    jQuery('.post-slider-2').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,

    });

    jQuery('.photo-video-slider').slick({
        infinite: true,
        arrows:false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
    });

    
    function slick_clone_remove() {
        
        var a = jQuery('.slick-cloned').children("a");

        jQuery('.slick-cloned').children("a").each(function (index, element) {
            var $element = jQuery(element);
            $element.removeAttr("data-fancybox");

        });
    }

    slick_clone_remove();
});

