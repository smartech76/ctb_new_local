<?php
/*
Template Name: Страница Отзывы
*/
get_header('page'); ?>
    <div class="w-auto banner-comments-background">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div class="title-banner-block d-flex justify-content-center flex-column">
                        <h1 class="text-white UniSans-Heavy font-size-6_5">ОТЗЫВЫ</h1>
                        <span class="text-white my-4 ">Мнение каждого туриста очень важно и ценно для нас Здесь вы найдете отзывы наших постоянных туристов и сможете поделиться своими впечатлениями об отдыхе и работе наших специалистов.</span>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-17">ОСТАВИТЬ ОТЗЫВ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <?php 
        $args = array(
            'cat' => 4,
            'paged' => get_query_var('paged'),
            'posts_per_page' => 10
        );
        query_posts($args);
        ?>
        <?php if (have_posts()) : ?>
          <?php while (have_posts()) : the_post(); ?>
                <div class="row py-4">
                   <div class="col-12 col-md-3 d-flex justify-content-center">
                       <div class="d-flex flex-column">
                            <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                            <span class="mt-3 font-size-13 font-weight-bold"></span>
                        </div>
                    </div>
                    <div class="col-12 col-md-1 my-3 my-md-0 d-flex justify-content-center">
                        <div>
                          <img src="<?php bloginfo("template_directory"); ?>/images/cc.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-8 d-flex flex-column mt-0">
					    <h3><?php the_title(); ?></h3>
                        <span class="font-weight-bold-title"><?php echo(get_post_meta($post->ID, 'date', true)); ?></span>	
                        <span class="font-weight-bold-title"><?php echo(get_post_meta($post->ID, 'name', true)); ?></span>
                        <span class="font-weight-bold"><?php the_excerpt(); ?></span>
						<div class="custom-modal-wrapper">
							<div class="modal fade" id="exampleModal<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
                                        <div class="modal-body">
                                            <div class="font-weight-bold"><?php the_content(); ?></div>
                                        </div>
								    </div>
								</div>
							</div>
						</div>	                 		
                   </div>
                </div>
            <?php endwhile; ?>
            <div class="container-panagination">
            <?php the_posts_pagination(); ?>
            </div>
        <?php else : ?>
            <h2>Записей нет</h2>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
        <?php wp_footer(); ?>
    </div>
<?php
get_footer('page');
?>