<?php
/*
Template Name: Страница Фото и Видео
*/
get_header('page');
?>
    <div class="w-100 banner-photo-background d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <div class="">
                        <h1 class="font-weight-bold-title font-size-5 text-white UniSans-Heavy">ФОТО<br>
                            И ВИДЕО
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 black-background-block">
        <div class="container position-relative container-imposing">
            <div class="row d-flex justify-content-center">
                <div class="col-sm-4 col-xs-12 px-0 position-relative w-auto">
                        <div>
                        <a target-fancybox-gallery="3"
                               class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/2.jpg"
                               data-fancybox="gallery-2"
                               data-thumb="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/2.jpg">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/222333.png">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/5.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/6.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/7.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/17.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/25.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/24.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/26.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/27.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/29.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/30.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/31.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-3"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-our-trip/32.jpg"
                               data-fancybox="gallery-2">
                            </a>
                        </div>
                     <div class="position-absolute title-thumbnail-box  d-flex align-items-center">
                         <span open-fancybox-gallery="3" class="font-weight-bold-title title-hover font-size-1_9 ml-2 UniSans-Heavy z-index-10">НАШИ ПОЕЗДКИ</span>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 px-0 position-relative w-auto">
                        <div>
                            <a target-fancybox-gallery="1"
                               class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/14.jpg"
                               data-fancybox="gallery-1"
                               data-thumb="<?php bloginfo("template_directory"); ?>/images/photo-tourists/14.jpg">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/photo-2.png"/>
                            </a>
                        </div>
                         <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/2.jpg"
                               data-fancybox="gallery-1"
                               data-thumb="<?php bloginfo("template_directory"); ?>/images/photo-tourists/2.jpg">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/3.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/4.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/5.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/6.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/7.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/8.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/9.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/10.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/11.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/12.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/13.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/14.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/15.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/16.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/17.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/18.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/19.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/20.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/21.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/22.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/23.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/24.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/26.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/27.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/28.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/29.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/30.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/31.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/32.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/33.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/34.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/35.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/36.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/37.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/photo-tourists/38.jpg"
                               data-fancybox="gallery-1">
                            </a>
                        </div>
                    <div class="position-absolute title-thumbnail-box  d-flex align-items-center">
                         <span open-fancybox-gallery="1" class="font-weight-bold-title title-hover font-size-1_9 ml-2 UniSans-Heavy z-index-10">ФОТО ТУРИСТОВ</span>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 px-0 position-relative w-auto">
                    <img src="<?php bloginfo("template_directory"); ?>/images/photo-3.png" class="img-fluid" alt="">
                    <div class="position-absolute title-thumbnail-box  d-flex align-items-center">
                    <span class="font-weight-bold-title title-hover font-size-1 ml-2 Open-Sans-Bold">ТОПы: отели, страны, города,
курорты, пляжи, достопримечательности</span>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-sm-4 col-xs-12 px-0 position-relative w-auto">
                    <img src="<?php bloginfo("template_directory"); ?>/images/photo-4.png" class="img-fluid" alt="">
                    <div class="position-absolute title-thumbnail-box  d-flex align-items-center">
                        <span class="font-weight-bold-title title-hover font-size-1_9 ml-2 UniSans-Heavy z-index-10">ВИДЕО</span>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 px-0 position-relative w-auto">
                    <div class="photo-video-slider mb-minus-6 d-none d-sm-block ">
                        <div>
                            <a target-fancybox-gallery="2" class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/capital-image/0ahZFBy7xCo.jpg"
                               data-fancybox="gallery">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/capital-image/0ahZFBy7xCo.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/capital-image/6ZZO5U1ULEc.jpg"
                               data-fancybox="gallery">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/capital-image/6ZZO5U1ULEc.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/capital-image/D1T4tK50AGI.jpg"
                               data-fancybox="gallery">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/capital-image/D1T4tK50AGI.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/capital-image/ffiC9YLdXRM.jpg"
                               data-fancybox="gallery">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/capital-image/ffiC9YLdXRM.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/capital-image/FyKp0hn0its.jpg"
                               data-fancybox="gallery">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/capital-image/FyKp0hn0its.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/capital-image/JN4eweys4ZM.jpg"
                               data-fancybox="gallery">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/capital-image/JN4eweys4ZM.png"/>
                            </a>
                        </div>
                        <div>
                            <a class="post-slider-1"
                               href="<?php bloginfo("template_directory"); ?>/images/capital-image/QAMO5NoakIk.jpg"
                               data-fancybox="gallery">
                                <img class="img-fluid"
                                     src="<?php bloginfo("template_directory"); ?>/images/capital-image/QAMO5NoakIk.png"/>
                            </a>
                        </div>
                    </div>
                    <img src="<?php bloginfo("template_directory"); ?>/images/photo-5.png" class="img-fluid d-sm-none" alt="">
                    <div class="position-absolute title-thumbnail-box  d-flex align-items-center">
                        <span open-fancybox-gallery="2" class="post-slider-1 font-weight-bold-title title-hover font-size-1_9 ml-2 UniSans-Heavy z-index-10">СТОЛИЦЫ МИРА
                        </span>
                    </div>
                </div>
                <div class="col-sm-4 px-0 position-relative w-auto">
                    <img src="<?php bloginfo("template_directory"); ?>/images/photo-6.png" class="img-fluid" alt="">
                    <div class="position-absolute title-thumbnail-box  d-flex align-items-center">
                        <span class="font-weight-bold-title title-hover font-size-1_9 ml-2 UniSans-Heavy z-index-10">УЛЫБНИСЬ</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer('page');
?>