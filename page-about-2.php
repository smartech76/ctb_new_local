<?php
/*
Template Name: Страница О нас (Разработка)
*/
get_header('page'); ?>
<div class="w100 banner-about-background">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-banner-block d-flex align-items-center">
                    <h1 class="text-white font-weight-bold-title font-size-6_5 UniSans-Heavy">О НАС</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container my-5">
    <div class="row">
        <div class="col-sm-12 col-md-12"> 
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="page-title"></h2>
				    <div class="entry clearfix content-img">
                        <?php the_post_thumbnail('medium', array('class' => 'alignleft')); ?>
                        <?php the_content(); ?>		
				    </div>
                        <?php wp_link_pages(); ?>
                        <?php edit_post_link(__( 'Edit', 'zeeTasty_lang' )); ?>
			</div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php
get_footer('about');
?>
