<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb_new
 */
wp_footer(); ?>
</body>
<footer>
    <div class="w-100 footer-background">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-6 d-flex flex-column">
                    <span class="text-gold font-weight-very-bold my-4 font-size-50 UniSans-Heavy">КОНТАКТЫ</span>
                    <div class="d-flex flex-column my-4 font-size-18">
                        <span class="text-gold font-weight-very-bold UniSans-Heavy">АДРЕС</span>
                        <span class="text-white">г. Ярославль, ул. Победы, д. 43/61</span>
                        <span class="text-white">ТЦ "МИГ" (у Макдональдса), 2-ой этаж</span>
                    </div>
                    <div class="d-flex flex-column my-3 font-size-18">
                        <span class="text-gold font-weight-very-bold UniSans-Heavy">E-MAIL</span>
                        <span class="text-white">info@ctb76.ru</span>
                    </div>
                    <div class="d-flex flex-column my-4 font-size-18">
                        <span class="text-gold font-weight-very-bold UniSans-Heavy">ТЕЛЕФОН / WHATSAPP / VIBER</span>
                        <span class="text-white">+7 (4852) 94-14-50</span>
                        <span class="text-white">+7 909 280 90 55</span>
                    </div>
                    <div class="d-flex flex-column font-size-18 d-sm-none my-4">
                        <span class="text-gold font-weight-very-bold UniSans-Heavy">СОЦИАЛЬНЫЕ СЕТИ</span>
                        <div class="mt-2">
                            <a href="https://vk.com/ctb76">
                                <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/vk.svg" alt="">
                            </a>
                            <a href="https://www.instagram.com/ctb76.ru/">
                                <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/instagram.svg" alt="">
                            </a>
                            <a href="https://www.facebook.com/groups/584779218582089/">
                                <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/facebook.svg" alt="">
                            </a>
                            <a href="https://ok.ru/group/55405382270984">
                                <img class="mx-2" src="<?php bloginfo("template_directory");?>/images/social-logo/odnoklassniki-logo.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-6 d-none d-md-flex align-items-center my-5">
                    <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A2fe6cc19d78323989fbaeafd0a3d35f5724d264e322698483080d3bcebee9733&amp;source=constructor" width="100%" height="455" frameborder="0"></iframe>
                </div>
            </div>
            <div class="row">
                <div class="col d-md-flex mb-0 mb-md-5">
                    <span class="text-white font-weight-very-bold footer-big-tur-word line-height-1 UniSans-Heavy">БОЛЬШЕ ЧЕМ<br> ТУРАГЕНСТВО</span>
                    <div class="d-md-flex justify-content-around col my-4 my-md-0">
                    <span class="text-white-no_impotant">
                    <ul class="pl-0 mb-0 mb-md-3">
                        <li>
                            <a href="<?php echo get_permalink(118); ?>" class="footer-menu-decorate">Фото и Видео</a>
                        </li>
                        <li class="pt-2">
                            <a href="<?php echo get_permalink(90); ?>" class="footer-menu-decorate">Поиск тура</a>
                        </li>
                        <li class="pt-2">
                            <a href="<?php echo get_permalink(92); ?>" class="footer-menu-decorate">Горящие</a>
                        </li>
                        <li class="pt-2">
                            <a href="<?php echo get_permalink(95); ?>" class="footer-menu-decorate">Раннее Бронирование</a>
                        </li>
                    </ul>
                    </span>
                        <span class="text-white-no_impotant">
                    <ul class="pl-0">
                        <li>
                            <a href="<?php echo get_permalink(97); ?>" class="footer-menu-decorate">Акции и Бонусы</a>
                        </li>
                        <li class="pt-2">
                            <a href="<?php echo get_permalink(100); ?>" class="footer-menu-decorate">Отзывы</a>
                        </li>
                        <li class="pt-2">
                            <a href="<?php echo get_permalink(103); ?>" class="footer-menu-decorate">Услуги</a>
                        </li>
                        <li class="pt-2">
                            <a href="<?php echo get_permalink(105); ?>" class="footer-menu-decorate">Программа привилегий</a>
                        </li>
                    </ul>
                </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {"apiSettings":{"root":"http:\/\/ctb76.ru\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"\u0411\u0443\u0434\u044c \u043b\u0430\u0441\u043a\u0430, \u043f\u0456\u0434\u0442\u0432\u0435\u0440\u0434\u0456\u0442\u044c, \u0449\u043e \u0432\u0438 \u043d\u0435 \u0440\u043e\u0431\u043e\u0442."}}};
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://ctb76.ru/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
</footer>
</html>
