<?php
/*
Template Name: Страница О нас 
*/
get_header('page'); ?>
<div class="w100 banner-about-background">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-banner-block d-flex align-items-center">
                    <h1 class="text-white font-weight-bold-title font-size-6_5 UniSans-Heavy">О НАС</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container my-5">
    <div class="row">
        <div class="col-sm-12 col-md-5">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/about-text.png" alt="">
        </div>
        <div class="col-sm-12 col-md-7 mt-4 mt-md-0 line-height-2 font-size-1">
            <p>Имея большой опыт в индустрии туризма, мы применяем современные маркетинговые инструменты в работе с
                клиентами и партнерами. Используя широкий спектр каналов привлечения новых клиентов, мы эффективно
                работаем над тем, чтобы они стали нашими постоянными туристами.</p>
            <p>И это у нас получается: общая база наших клиентов уже насчитывает более 20 000 человек и регулярно
                увеличивается. Залог успеха нашего дела – это, конечно, команда профессионалов! Молодой и дружный
                коллектив нашей компании всегда открыт к новым идеям и нестандартным подходам.</p>
            <p>Мы не просто работаем вместе, мы легко можем за пару дней собрать чемоданы и махнуть на лазурный берег в
                поисках приключений!</p>
        </div>
    </div>
</div>
<div class="w100 about-two-block">
    <div class="container">
        <div class="row">
            <div class="col d-flex justify-content-center py-5">
                <h2 class="font-weight-bold-title font-size-50 text-gold UniSans-Heavy">ПОЧЕМУ МЫ ?</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg d-flex align-items-center flex-column text-align-center pb-4 pb-lg-0">
                <div class=" d-flex justify-content-center">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/about-1.png" alt="">
                </div>
                <span class="font-weight-very-bold font-size-1 my-5 text-gold UniSans-Heavy">ПРОФЕССИОНАЛЬНО</span>
                <p class="text-white px-4">Наши специалисты работают в сфере туризма более 15 лет.</p>
                <div class="separator w-25 py-2"></div>
            </div>
            <div class="col-sm-12 col-lg d-flex align-items-center flex-column text-align-center py-4 py-lg-0">
                <div class=" d-flex justify-content-center">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/about-2.png" alt="">
                </div>
                <span class="font-weight-very-bold font-size-1 my-5 text-gold UniSans-Heavy">ЛЮБАЯ ФОРМА ОПЛАТЫ</span>
                <p class="text-white px-4">Возможность оплаты по карте и рассрочка до 4 месяцев.</p>
                <div class="separator w-25 py-2"></div>
            </div>
            <div class="col-sm-12 col-lg d-flex align-items-center flex-column text-align-center py-4 py-lg-0">
                <div class=" d-flex justify-content-center">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/about-3.png" alt="">
                </div>
                <span class="font-weight-very-bold font-size-1 my-5 text-gold UniSans-Heavy">НАДЕЖНО</span>
                <p class="text-white px-4">Работаем только с проверенными туроператорами.</p>
                <div class="separator w-25 py-2"></div>
            </div>
            <div class="col-sm-12 col-lg d-flex align-items-center flex-column text-align-center py-4 py-lg-0">
                <div class="d-flex justify-content-center">
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/about-4.png" alt="">
                </div>
                <span class="font-weight-very-bold font-size-1 my-5 text-gold UniSans-Heavy">ЛУЧШАЯ ЦЕНА</span>
                <p class="text-white px-4">Работаем только по спецпредложениям и выгодным ценам</p>
                <div class="separator w-25 py-2"></div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col d-flex justify-content-center py-5">
            <h2 class="text-grey font-weight-very-bold font-size-50 UniSans-Heavy">НАША КОМАНДА</h2>
        </div>
    </div>
    <div class="row py-4">
        <div class="col-sm-12 col-md-3 text-align-center pb-3 pb-md-0">
            <img class=" img-fluid" src="<?php bloginfo("template_directory");?>/images/Popova-person.jpg" alt="">
        </div>
        <div class="pl-5 d-flex flex-column justify-content-center col-sm-12 col-md-9">
            <span class="pb-3 text-brown font-weight-very-bold UniSans-Heavy">ЮЛИЯ ПОПОВА</span>
            <span class="text-grey-light">Генеральный директор</span>
            <span class="pt-3">Основатель, лидер и душа компании. Красный диплом с отличием профильного ВУЗа и более, чем 15 лет в туризме. Огромный опыт, знание многочисленной клиентской базы в лицо; чуткое отношение к клиентам, коллегам и подчинённым; неуёмное желание достичь совершенства во всех аспектах деятельности турагентсва.</span>

        </div>
    </div>
    <div class="row py-4">
        <div class="col-sm-12 col-md-3 text-align-center pb-3 pb-md-0">
            <img class=" img-fluid" src="<?php bloginfo("template_directory");?>/images/Morozova-person.jpg" alt="">
        </div>
        <div class=" pl-5 d-flex flex-column justify-content-center col-sm-12 col-md-9">
            <span class="pb-3 text-brown font-weight-very-bold UniSans-Heavy">ОЛЬГА МОРОЗОВА</span>
            <span class="text-grey-light">Директор по развитию</span>
            <span class="pt-3">За шесть лет работы менеджером по продажам и ведущим менеджером, шагнула на новую ступень. Знает специфику работы изнутри. Имеет большой опыт по организации и продвижению крупных федеральных проектов. В курсе всех основных трендов индустрии.</span>

        </div>
    </div>
    <div class="row py-4">
        <div class="col-sm-12 col-md-3 text-align-center pb-3 pb-md-0">
            <img class=" img-fluid" src="<?php bloginfo("template_directory");?>/images/Yakovleva-person.jpg" alt="">
        </div>
        <div class="pl-5 d-flex flex-column justify-content-center col-sm-12 col-md-9">
            <span class="pb-3 text-brown font-weight-very-bold UniSans-Heavy">АННА ЯКОВЛЕВА</span>
            <span class="text-grey-light">Менеджер по выездному туризму и индивидуальным турам</span>
            <span class="pt-3">Прошла обучение в профильном ВУЗе, имеет за спиной более 20 стран, в которых лично побывала в качестве туриста. Регулярно проходит тренинги по технологиям работы с клиентами и профильные обучающие программы. Неоднократно была удостоена звания лучший менеджер месяца.</span>
        </div>

    </div>
    <div class="row py-4">
        <div class="col-sm-12 col-md-3 text-align-center pb-3 pb-md-0">
            <img class=" img-fluid" src="<?php bloginfo("template_directory");?>/images/Gribanova-person.jpg" alt="">
        </div>
        <div class="pl-5 d-flex flex-column justify-content-center col-sm-12 col-md-9">
            <span class="pb-3 text-brown font-weight-very-bold UniSans-Heavy">ЛЮБОВЬ ГРИБАНОВА</span>
            <span class="text-grey-light">Менеджер по выездному туризму и индивидуальным турам</span>
            <span class="pt-3">Живой, общительный, по-хорошему въедливый, любящий свою работу специалист. На постоянной основе повышает свой профессионализм, изучая как самые популярные страны и курорты, так и молодые, развивающиеся направления. Имеет в своей копилке множество направлений, где побывала в рекламных турах и на отдыхе.</span>

        </div>
    </div>
    <div class="row py-4">
        <div class="col-sm-12 col-md-3 text-align-center pb-3 pb-md-0">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/Musinova-person.jpg" alt="">
        </div>
        <div class="pl-5 d-flex flex-column justify-content-center col-sm-12 col-md-9">
            <span class="pb-3 text-brown font-weight-very-bold UniSans-Heavy">ИРИНА МУСИНОВА</span>
            <span class="text-grey-light">Менеджер по выездному туризму и индивидуальным турам</span>
            <span class="pt-3">Ответственная, клиенториетированная, располагающая к себе любого клиента. Активная жизненная позиция и стремление к постоянному саморазвитию. Обладает обширными знаниями в области туризма, до мельчайших деталей может рассказать о специфике отдыха на том или ином курорте.</span>

        </div>
    </div>

</div>
<?php
get_footer('about');
?>
