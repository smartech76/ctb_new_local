<?php
/*
Template Name: Страница Горячие
*/
get_header('page');
?>
    <div class="w-100 banner-hot-background">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title-banner-block d-flex justify-content-center flex-column">
                        <h1 class="text-white font-weight-very-bold font-size-5 UniSans-Heavy font-size-5-md">ГОРЯЧИЕ <br> СПЕЦПРЕДЛОЖЕНИЯ</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm my-5">
                <script type="text/javascript" src="https://ui.sletat.ru/module-4.0/core.js" charset="utf-8"></script>
                <script type="text/javascript">sletat.FrameHot.$create({
                        toursCount: 8,
                        useCard: false,
                        agencyContact2: {
                            header: "ЦТБ - Центр Туристического Бронирования",
                            phone: "+7(4852) 94-14-50",
                            email: "info@ctb76.ru"
                        },
                        enabledCurrencies: ["RUB"]
                    });</script>
                <span class="sletat-copyright">Идет загрузка модуля <a href="https://sletat.ru/" title="поиск туров"
                                                                       target="_blank">поиска туров</a> &hellip;</span>
            </div>
        </div>
    </div>
    <div class="w-100 black-background-block">
        <div class="container">
            <div class="row">
                <div class="col d-flex justify-content-center py-5">
                    <h2 class=" font-weight-very-bold text-gold font-size-4 UniSans-Heavy">ВЫБЕРИ СВОЙ ВАРИАНТ</h2>
                </div>
            </div>
            <div class="row pb-5">
                <div class="col-sm-12 col-md block-hot-one">
                    <div class="d-flex justify-content-start m-4">
                        <div class="text-white px-3 py-2 hot-block-one-number-border font-weight-bold-title font-size-2_9 UniSans-Heavy">01</div>
                    </div>
                    <div class="p-4">
                        <span class="text-white font-size-2_9">Хочу получить подборку горящих туров</span>
                    </div>
                    <div class="p-4">
                        <button type="button" class="text-white btn button-red-style mt-5 font-size-18 btn-lg eModal-14">ПОЛУЧИТЬ ПОДБОРКУ</button>
                    </div>
                </div>
                <div class="col-sm-12 col-md block-hot-two">
                    <div class="d-flex justify-content-start m-4">
                        <div class="text-white px-3 py-2 hot-block-one-number-border font-weight-bold-title font-size-2_9 UniSans-Heavy">02</div>
                    </div>
                    <div class="p-4">
                        <span class="text-white font-size-2_9">Посмотреть последние спецпредложения</span>
                    </div>
                    <div class="p-4">
                        <a href="https://vk.cc/7ZvQvp" target="_blank" class="text-white btn button-red-style mt-5 font-size-18 btn-lg">ПОСМОТРЕТЬ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col d-flex flex-column justify-content-start">
                <h2 class="font-weight-very-bold UniSans-Heavy">ЧТО ТАКОЕ ГОРЯЩИЕ?</h2>
                <span>Это туры, на которые снижаются цены ближе к датам поездки. Происходит это в том случае, когда туроператор заранее выкупил авиабилеты, номера в отеле, но не продал полную квоту. И вот остаются последние два билета на самолёт и лучше продать дёшево, чем не продать совсем. Таким образом появляются спецпредложения, но угадать заранее по какому направлению, на сколько человек и в какой отель будут скидки - просто нет шансов. 
                Чаще речь идет о том, что туристы по тем или иным причинам не купили (неудобное время вылетов, плохие отзывы по отелю, ненадежный туроператор и пр.) НО ТАК БЫЛО РАНЬШЕ!!! 
                Сейчас туроператоры отказываются от практики выкупа квот заранее в пользу динамического пакетирования: когда турпакеты формируются в режиме реального времени, билеты и отели продаются под запрос. У туроператоров отсутствуют риски не продать! Гореть просто НЕЧЕМУ!  Горящие изживают себя как вид!
                </span>
            </div>
        </div>
    </div>
    <div class="w-100 py-5 earlier-booking-background-block-grey">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-column justify-content-start">
                    <span class="font-weight-very-bold font-size-2 UniSans-Heavy">МИФЫ ПРО ГОРЯЩИЕ:</span>
                    <span>Многие думают, что это отказные туры, уже купленные кем-то, кто не может поехать. Это миф, это не так!</span>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col d-flex flex-column justify-content-start">
                <span class="font-weight-very-bold font-size-2 UniSans-Heavy">ПРЕИМУЩЕСТВА</span>
                <span>Можно ожидать реально снижения цены.</span>
            </div>
        </div>
    </div>
    <div class="w-100 py-5 earlier-booking-background-block-grey">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-column justify-content-start">
                    <span class="font-weight-very-bold font-size-2 UniSans-Heavy">НЮАНСЫ</span>
                    <span>Горящие, как правило, появляются за день-два до вылета и предугадать, на какие даты будут скидки, нереально. Надо быть не привязанным к отпуску и с определенной долей авантюризма быть готовым собраться за считаные часы и помчаться в аэропорт.
                    Горящих НЕ БЫВАЕТ на любые праздники (Новый год, 8 марта и т.д.) и в период школьных каникул.
                    Гореть может тур за 150.000 руб., когда он стоил 300.000 руб. – отличная скидка в 50%! Если Ваш бюджет совсем мал, то и шансы на горящие не велики.
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col d-flex flex-column justify-content-start">
                <span class="font-weight-very-bold font-size-2 UniSans-Heavy">АЛЬТЕРНАТИВЫ</span>
                <span>Раннее Бронирование (РБ) - это идеальный вариант для тех, кто не готов переплачивать за отдых! Скидки по РБ доходят до 50%, при этом Вы спокойно заранее планируете свою поездку, выбираете подходящий именно для Вас тур. Подробнее о том, что такое Акции Раннего Бронирования читайте на <a href="https://ctb76.ru/rannee-bronirovanie/">нашем сайте.</a></span>
            </div>
        </div>
    </div>
<?php
get_footer('page');
?>