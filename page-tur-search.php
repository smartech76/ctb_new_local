<?php
/*
Template Name: Страница Подбор тура
*/
get_header('page');
?>
<div class="w-auto banner-hot-background">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-banner-block d-flex justify-content-center flex-column">
                    <h1 class="text-white font-weight-very-bold font-size-5_5 UniSans-Heavy">ВЫБРАТЬ ТУР</h1>
                    <span class="mt-4 text-white font-size-1_2">Выбор идеального тура для путешествия - это большая работа, в которой много важных нюансов: надежный туроператор, проверенная авиакомпания, удобное время вылетов, отвечающий всем требованиям отель, выбор оптимальных условий страхования и многое многое другое. Это наша работа и мы делаем ее качественно и профессионально!<br>
                    Вы можете воспользоваться системой поиска на нашем сайте или отправить нам запрос на подбор тура.</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="w-100 black-background-block">
    <div class="container">
        <div class="row">
            <div class="col my-5">
                <script type="text/javascript" src="//ui.sletat.ru/module-5.0/app.js" charset="utf-8"></script>
                <script type="text/javascript">sletat.createModule5('Search', {
                        files            : ["//ui.sletat.ru/module-5.0/theme/sea_dec2015.min.css"],
                        files            : ["<?php bloginfo("template_directory");?>/css/fraim.css"],
                        enabledCurrencies : ["RUB", "EUR", "USD"],
                        useCard           : false,
                    });</script>
                <span class="sletat-copyright">Идет загрузка модуля <a href="//sletat.ru/" title="поиск туров" target="_blank">поиска туров</a> &hellip;</span>
            </div>
        </div>
    </div>
</div>
<?php
get_footer('page');
?>