<?php
/*
Template Name: Страница Раннее бронирование
*/

get_header('page');
?>

    <div class="w-auto banner-earlier-background">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title-banner-block d-flex justify-content-center flex-column">
                        <h1 class="text-white font-weight-bold-title UniSans-Heavy font-size-5 font-size-5-md">РАННЕЕ
                            БРОНИРОВАНИЕ</h1>
                        <span class="text-gold my-4 font-weight-bold-title UniSans-Heavy font-size-2">ПЛАТИ РАНЬШЕ, НО МЕНЬШЕ</span>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-16">ПОЛУЧИТЬ ПОДБОРКУ ТУРОВ
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="w-100 py-5 earlier-booking-background-block-grey">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-column justify-content-start">
                    <span class="font-weight-bold-title UniSans-Heavy font-size-2">15 ПОВОДОВ КУПИТЬ ТУР</span>
                    <span>
                       15 реальных поводов купить тур по раннему бронированию.<br>
                Отпуск – чудесная пора! Мы его ждём, о нём мечтаем, его планируем, придумываем место, время, наполнение. Даже от самой любимой работы на свете всем хочется отдохнуть. Наша рекомендация – подумать об отпуске заранее и приобрести путёвку по акциям Раннего Бронирования (РБ). ПОЧЕМУ?<br>
                <span class="font-weight-bold-title">Повод №1: Экономия бюджета на отдых до 50%.</span><br>
                Действительно, по раннему бронированию отели дают очень приличные скидки. Им выгодно продать заранее, поэтому контрактные
                    </span>
                    <div class="mt-3">
                    <a href="<? echo get_permalink(488) ?>" class="text-white btn button-red-style"> Читать далее...
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col d-flex flex-column justify-content-start">
                <h2 class="font-weight-bold-title UniSans-Heavy font-size-2">ЧТО ТАКОЕ РАННЕЕ БРОНИРОВАНИЕ (РБ)?</h2>
                <span>Акции РБ - это сезонные предложения туроператоров, когда те же самые туры ЗАРАНЕЕ стоят ДЕШЕВЛЕ, чем перед вылетом</span>
            </div>
        </div>
    </div>
    <div class="w-100 py-5 earlier-booking-background-block-grey">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-column justify-content-start">
                    <span class="font-weight-bold-title UniSans-Heavy font-size-2">ПОЧЕМУ НАДО ПОКУПАТЬ?</span>
                    <span>
                        <ul>
                            <li>если вы хотите сэкономить</li>
                            <li>если вы хотите иметь широкий выбор предложений (отелей, авиакомпаний, рейсов) </li>
                            <li>если вы хотите гарантированно отдохнуть, особенно в высокий сезон и на популярном курорте </li>
                            <li>если ваш отпуск привязан к четким датам</li>
                        </ul>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col d-flex flex-column justify-content-start">
                <span class="font-weight-bold-title UniSans-Heavy font-size-2">КОГДА ПОКУПАТЬ?</span>
                <span>За 3-6 месяцев до начала поездки.</span>
            </div>
        </div>
    </div>
    <div class="w-100 py-5 earlier-booking-background-block-grey">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-column justify-content-start">
                    <span class="font-weight-bold-title UniSans-Heavy font-size-2">ЧТО ПОКУПАТЬ?</span>
                    <span>Летом - на зиму: <br>
экзотика: Тайланд, Индия, Вьетнам, ОАЭ, Куба, Доминикана, Мексика, новый год, горные лыжи, школьные каникулы и др. <br>
Зимой - на лето: <br>
майские праздники, Турция, Греция, Кипр, Болгария, Крым, Краснодарский край и др.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col d-flex flex-column justify-content-start">
                <span class="font-weight-bold-title UniSans-Heavy font-size-2">КАКИЕ УСЛОВИЯ?</span>
                <span>Каждый туроператор выставляет свои требования по оплате путевки. Как правило, первый платеж составляет 1/3 от стоимости тура; до окончания действия акции необходимо оплатить 1/2 часть, срок полной оплаты может быть за 2 недели до вылета.</span>
            </div>
        </div>
    </div>
<?php
get_footer('page');
?>