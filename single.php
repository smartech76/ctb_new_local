<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ctb_new
 */
get_header('page');
?>
<div class="w-100" style="background: url(<? echo get_the_post_thumbnail_url() ?>) no-repeat; background-size:cover; height: 36rem">
    <div class="container h-100 w-100 d-flex align-items-center"  >
        <div class="row">
            <div class="col">
                <h1 class="font-weight-very-bold text-white UniSans-Heavy d-none d-md-block font-size-5_5"><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
</div>
<div class="container pt-4">
    <div class="row">
        <?php
        while (have_posts()) :
            the_post(); ?>
            <div class="col">
                <div class="text-center">
                    <h2 class="d-md-none text-gold UniSans-Heavy"><?php the_title(); ?></h2>
                </div>
                <div class="reading-time">
                    <div class="font-weight-bold-title">
                        <span class="text-blue"><img class="time-img" src="<?php bloginfo("template_directory"); ?>/images/time-img.png"/>Время чтения: </span><?php echo(get_post_meta($post->ID, 'Время чтения', true)); ?>
                    </div>
                    <div class="font-weight-bold-title">
                        <span class="text-blue">Дата публикации: </span><?php echo(get_post_meta($post->ID, 'Дата публикации', true)); ?>
                    </div>
                    <div class="font-weight-bold-title">
                        <span class="text-blue">Автор: </span><?php echo(get_post_meta($post->ID, 'Автор', true)); ?>
                    </div>
                </div>
                <div class="content-img">
                    <?php the_content(); ?>
                </div>
            </div>
        <? endwhile; ?>
    </div>
</div>
<?php
get_footer('page');
?>
