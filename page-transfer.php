<?php
/*
Template Name: Страница Трансфер
*/
get_header('page');
?>
    <div class="w-100 banner-transfer-background">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="title-banner-block d-flex justify-content-center flex-column" id="transfer-banner-title">
                        <h1 class="text-white font-weight-very-bold font-size-5 UniSans-Heavy">ЗАКАЗАТЬ ТРАНСФЕР</h1>
                        <div class="d-md-flex d-block font-size-1_2 justify-content-around pt-5 ">
                    <span class="text-white">
                    <ul class="font-weight-bold  mb-0">
                        <li>Удобная форма заказа и оплаты онлайн</li>
                        <li>Демократичные цены</li>
                        <li>Гибкое расписание</li>
                    </ul>
                    </span>

                            <span class="text-white">
                    <ul class="font-weight-bold">
                        <li>Групповые и индивидуальные перевозки</li>
                        <li>С места сбора или от дома</li>
                        <li>Комфортабельные автомобили</li>
                    </ul>
                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="w-100 black-background-block">
<div class="container">
    <div class="row">
        <div class="col py-4">
            <iframe src="https://transfer.yarobltour.ru/bron/bron_modern.php"; width="100%"
                    height="650" frameborder="0"></iframe>
        </div>
    </div>
</div>
</div>
<?php
get_footer('page');
?>