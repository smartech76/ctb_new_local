<?php
/*
Template Name: Страница Услуги
*/
get_header('page'); ?>
<div class="w-auto banner-service-background">
    <div class="container">
        <div class="row">
            <div class="col-8">
                <div class="title-banner-block d-flex justify-content-center flex-column">
                    <h1 class="text-white font-weight-bold-title mb-5 UniSans-Heavy font-size-6_5">УСЛУГИ</h1>
                    <div>
                        <img src="<?php bloginfo("template_directory");?>/images/soon.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col d-flex justify-content-center h-250">
            <div class="position-relative bottom-2_7">
                <ul class="nav nav-tabs font-weight-very-bold " id="ServiceTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active UniSans-Heavy text-white-no_impotant text-hover-gold" id="tours_abroad-tab"
                           data-toggle="tab" href="#tours_abroad" role="tab"
                           aria-controls="tours_abroad" aria-selected="true">ТУРЫ ЗА ГРАНИЦУ</a>
                    </li>
                    <li class="nav-item d-none d-md-block ">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="russia_tours-tab"
                           data-toggle="tab" href="#russia_tours" role="tab"
                           aria-controls="russia_tours" aria-selected="false">ТУРЫ ПО РОССИИ</a>
                    </li>
                    <li class="nav-item d-none d-md-block ">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="air_ticket-tab"
                           data-toggle="tab" href="#air_ticket" role="tab"
                           aria-controls="air_ticket" aria-selected="false">АВИАБИЛЕТЫ</a>
                    </li>
                    <li class="nav-item d-none d-xl-block">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="transfer-tab" data-toggle="tab"
                           href="#transfer" role="tab"
                           aria-controls="transfer" aria-selected="false">ТРАНСФЕРЫ</a>
                    </li>
                    <li class="nav-item  d-none d-lg-block">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="hotel-tab" data-toggle="tab"
                           href="#hotel" role="tab" aria-controls="hotel"
                           aria-selected="false">ОТЕЛИ</a>
                    </li>
                    <li class="nav-item d-none d-xl-block">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="insurance-tab" data-toggle="tab"
                           href="#insurance" role="tab"
                           aria-controls="insurance" aria-selected="false">СТРАХОВАНИЕ</a>
                    </li>
                    <li class="nav-item d-none d-xl-block">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="cruise-tab" data-toggle="tab"
                           href="#cruise" role="tab"
                           aria-controls="cruise" aria-selected="false">КРУИЗЫ</a>
                    </li>
                    <li class="nav-item d-none d-xl-block">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="visa-tab" data-toggle="tab"
                           href="#visa" role="tab" aria-controls="visa"
                           aria-selected="false">ВИЗЫ</a>
                    </li>
                    <li class="nav-item d-none d-xl-block">
                        <a class="nav-link UniSans-Heavy text-white-no_impotant text-hover-gold" id="other-tab" data-toggle="tab"
                           href="#other" role="tab" aria-controls="other"
                           aria-selected="false">ПРОЧЕЕ</a>
                    </li>
                    <li class="nav-item dropdown d-none d-lg-block d-xl-none " id="1-block">
                        <a class="nav-link UniSans-Heavy dropdown-toggle text-white-no_impotant text-hover-gold"
                           data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false">ОСТАЛЬНОЕ</a>
                        <div class="dropdown-menu">

                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#insurance">СТРАХОВАНИЕ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#cruise">КРУИЗЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#visa">ВИЗЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#other">ПРОЧЕЕ</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown d-none d-md-block d-lg-none " id="2-block">
                        <a class="nav-link UniSans-Heavy dropdown-toggle text-white-no_impotant text-hover-gold"
                           data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false">ОСТАЛЬНОЕ</a>
                        <div class="dropdown-menu">

                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#transfer">ТРАНСФЕРЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#hotel">ОТЕЛИ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#insurance">СТРАХОВАНИЕ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#cruise">КРУИЗЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#visa">ВИЗЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#other">ПРОЧЕЕ</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown d-md-none " id="3-block">
                        <a class="nav-link dropdown-toggle text-white-no_impotant text-hover-gold"
                           data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false">ОСТАЛЬНОЕ</a>
                        <div class="dropdown-menu">
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#russia_tours">ТУРЫ ПО РОССИИ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#air_ticket">АВИАБИЛЕТЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#transfer">ТРАНСФЕРЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#hotel">ОТЕЛИ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#insurance">СТРАХОВАНИЕ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#cruise">КРУИЗЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#visa">ВИЗЫ</a>
                            <a class="nav-link UniSans-Heavy dropdown-item text-white-no_impotant text-hover-gold" role="tab"
                               data-toggle="tab" href="#other">ПРОЧЕЕ</a>
                        </div>
                    </li>
                </ul>

                <div class="tab-content my-3" id="ServiceTabContent">
                    <div class="tab-pane fade show active" id="tours_abroad" role="tabpanel"
                         aria-labelledby="tours_abroad-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>Пакетные</li>
                                <li>Экскурсионные</li>
                                <li>Индивидульные</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="russia_tours" role="tabpanel" aria-labelledby="russia_tours-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>Эксркурсионные</li>
                                <li>Однодневные</li>
                                <li>Ж/д, автобус на Юг</li>
                                <li>Речные круизы</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="air_ticket" role="tabpanel" aria-labelledby="air_ticket-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>Чартерные</li>
                                <li>Регулярные</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="transfer" role="tabpanel" aria-labelledby="transfer-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>Групповые</li>
                                <li>Индивидуальные</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>Россия</li>
                                <li>Заграница</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="insurance" role="tabpanel" aria-labelledby="insurance-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>Медицина</li>
                                <li>От невыезда</li>
                                <li>Любые риски</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="cruise" role="tabpanel" aria-labelledby="cruise-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>Морские</li>
                                <li>Речные</li>
                                <li>Межконтинентальные</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="visa" role="tabpanel" aria-labelledby="visa-tab">
                        <div class="d-flex">
                            <ul class="pl-3">
                                <li>С оформлением в Москве</li>
                                <li>Ярославле</li>
                                <li>Вологде</li>
                            </ul>
                        </div>
                        <div>
                            <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ</button>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="other" role="tabpanel" aria-labelledby="other-tab">
                        <div class="d-flex flex-column">
                            <div class="d-sm-flex">
                                <ul class="pl-3">
                                    <li>Лечение и профилактика за границей</li>
                                    <li>Праздничный и событийный туризм</li>
                                    <li>Для любителей экстрима и активного отдыха</li>
                                    <li>Туры для организованных групп</li>
                                </ul>
                                <ul class="pl-5">
                                    <li>MICE (деловой туризм)</li>
                                    <li>Шоп-туры</li>
                                    <li>Образование за рубежом</li>
                                    <li>Любые нестандартные запросы</li>
                                </ul>
                            </div>
                            <div>
                                <button type="button" class="text-white btn button-red-style eModal-13">ПОЛУЧИТЬ КОНСУЛЬТАЦИЮ
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer('page');
?>