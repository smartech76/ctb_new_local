<?php
/*
Template Name: Страница Партнёры
*/
get_header('page');
?>
<div class="w-100 banner-partners-background">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-banner-block d-flex justify-content-center flex-column">
                    <h1 class="text-white font-weight-bold-title font-size-5 UniSans-Heavy">ПАРТНЁРЫ</h1>
                    <span class="text-gold my-4 font-weight-bold-title font-size-2 UniSans-Heavy">РАБОТАЕМ ТОЛЬКО С ПРОВЕРЕННЫМИ ТУРОПЕРАТОРАМИ</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container text-align-center">
    <div class="row ">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BIBGLO.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_ANEX.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_VIking.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Pegas.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Troyka.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_SanMar.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Coral.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_TezT.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_IntTur.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Allean.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Mouze.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Tui.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Paks.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_RusEx.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_AMBOTIS.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_DVM_T.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_TTVoyage.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Dolph.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_ICS.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_AMIGOS.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_AMIGOTU.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_ANKOR.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_BALKAN.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Danko.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_WEST.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_Multi.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_RASGr.png" alt="">
        </div>
        <div class="col-sm-12 col-md-3 my-4">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/logo-partners/CTB_sait_logo_VEDI.png" alt="">
        </div>
    </div>
</div>
<?php
get_footer('page');
?>