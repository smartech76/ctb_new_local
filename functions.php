<?php
/**
 * ctb_new functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ctb_new
 */

if ( ! function_exists( 'ctb_new_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function ctb_new_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ctb_new, use a find and replace
		 * to change 'ctb_new' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'ctb_new', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
        if (function_exists('add_theme_support')) {
            add_theme_support('menus');
        }

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'ctb_new_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
		
		//Adds a change to the length of the displayed announcement
        add_filter( 'excerpt_length', function(){
	     return 70;
        } );
        add_filter('excerpt_more', function($more) {
			
			global $post;
			$str_excerpt_more = "";
			$str_post_content = $post->post_content;
						
			if (strlen($str_post_content) > 900) {
				$id = $post->ID;
				$str_excerpt_more = '<a href="/" data-toggle="modal" data-target="#exampleModal' . $id . '"><читать далее></a>';
			}
			
			return $str_excerpt_more;
        });
		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'ctb_new_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ctb_new_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'ctb_new_content_width', 640 );
}
add_action( 'after_setup_theme', 'ctb_new_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ctb_new_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ctb_new' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ctb_new' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

/**
 * Enqueue style
 */

function ctb_new_style () {
    wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.css');
    wp_register_style('page-css', get_template_directory_uri() . '/css/page.css');
    wp_register_style('slick-css', get_template_directory_uri() . '/slick/slick.css');
    wp_register_style('slick-theme-css', get_template_directory_uri() . '/slick/slick-theme.css');
    wp_register_style('menu-css', get_template_directory_uri() . '/css/menu.css');
    wp_register_style('menu-css-2', get_template_directory_uri() . '/css/menu-2.css');
    wp_register_style('fancybox-css', get_template_directory_uri() . '/fancybox/dist/jquery.fancybox.css');


    wp_enqueue_style( 'bootstrap-css');
    wp_enqueue_style( 'slick-css');
    wp_enqueue_style( 'slick-theme-css');
    wp_enqueue_style( 'menu-css');
    wp_enqueue_style( 'menu-css-2');
    wp_enqueue_style( 'fancybox-css');
    wp_enqueue_style( 'page-css');
}

add_action('wp_enqueue_scripts', 'ctb_new_style');

/**
 * Enqueue scripts
 */

function ctb_new_scripts() {
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js');
    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.js');
    wp_register_script('bootstrap-bundle-js', get_template_directory_uri() . '/js/bootstrap.bundle.js');
    wp_register_script('initialization-slide', get_template_directory_uri() . '/js/sliders-init.js');
    wp_register_script('subscription-button', get_template_directory_uri() . '/js/subscription.js');
    wp_register_script('slick-js', get_template_directory_uri() . '/slick/slick.js');
    wp_register_script('target-paralax', get_template_directory_uri() . '/js/target-script.js');
    wp_register_script('jquery-paralax', get_template_directory_uri() . '/js/jquery.parallax.js');
    wp_register_script('menu-js', get_template_directory_uri() . '/js/menu.js');
    wp_register_script('menu-lib', get_template_directory_uri() . '/js/modernizr.custom.js');
    wp_register_script('menu-class', get_template_directory_uri() . '/js/classie.js');
    wp_register_script('fancybox-js', get_template_directory_uri() . '/fancybox/dist/jquery.fancybox.js', array('jquery'));
	wp_register_script('photo', get_template_directory_uri() . '/js/photo.js');

    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap-bundle-js');
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('subscription-button');
    wp_enqueue_script('menu-class');
    wp_enqueue_script('menu-js');
    wp_enqueue_script('slick-js');
    wp_enqueue_script('jquery-paralax');
    wp_enqueue_script('target-paralax');
    wp_enqueue_script('initialization-slide');
    wp_enqueue_script('menu-lib');
    wp_enqueue_script('fancybox-js');
	wp_enqueue_script('photo');

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'ctb_new_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_image_size( '', '100%', '100%', true );
