<?php
/**
 * The template for displaying the footer in page
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb_new
 */
wp_footer(); ?>
</body>
<footer>
    <div class="w-100 footer-page-background">
        <div class="container">
            <div class="row">
                <div class="col d-md-flex mb-0 my-md-5">
                    <span class="text-white font-weight-very-bold footer-big-tur-word line-height-1 UniSans-Heavy">БОЛЬШЕ ЧЕМ<br> ТУРАГЕНСТВО</span>
                    <div class="d-md-flex flex-column col my-4 my-md-0">
                        <div class="d-md-flex justify-content-around">
                            <span class="text-white-no_impotant">
                                <ul class="pl-0 mb-0 mb-md-3">
                                    <li><a href="<?php echo get_permalink(118); ?>" class="footer-menu-decorate">Фото и Видео</a></li>
                                    <li><a href="<?php echo get_permalink(90); ?>" class="footer-menu-decorate">Поиск тура</a></li>
                                    <li><a href="<?php echo get_permalink(92); ?>" class="footer-menu-decorate">Горящие</a></li>
                                    <li><a href="<?php echo get_permalink(95); ?>" class="footer-menu-decorate">Раннее Бронирование</a></li>
                                </ul>
                            </span>
                            <span class="text-white-no_impotant">
                                <ul class="pl-0">
                                    <li><a href="<?php echo get_permalink(97); ?>" class="footer-menu-decorate">Акции и Бонусы</a></li>
                                    <li><a href="<?php echo get_permalink(100); ?>" class="footer-menu-decorate">Отзывы</a></li>
                                    <li><a href="<?php echo get_permalink(103); ?>" class="footer-menu-decorate">Услуги</a></li>
                                    <li><a href="<?php echo get_permalink(105); ?>" class="footer-menu-decorate">Программа привилегий</a></li>
                                </ul>
                            </span>
                        </div>
                        <div class="d-flex justify-content-center">
                            <span class="text-gold font-size-1_2 font-weight-very-bold UniSans-Heavy">ООО "ЦЕНТР ТУРИСТИЧЕСКОГО БРОНИРОВАНИЯ"</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wpcf7 = {"apiSettings":{"root":"http:\/\/ctb76.ru\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"\u0411\u0443\u0434\u044c \u043b\u0430\u0441\u043a\u0430, \u043f\u0456\u0434\u0442\u0432\u0435\u0440\u0434\u0456\u0442\u044c, \u0449\u043e \u0432\u0438 \u043d\u0435 \u0440\u043e\u0431\u043e\u0442."}}};
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://ctb76.ru/wp-content/plugins/contact-form-7/includes/js/scripts.js'></script>
</footer>
</html>