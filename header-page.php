<?php
/**
 * The header for page
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ctb_new
 */
?>

<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head(); ?>
    <meta name="KeyWords"
          content="Центр Туристического Бронирования ярославль, ЦТБ, горящие путевки, ЦТБ поиск туров, горящие путевки, горящие путевки Ярославль, горящие туры, подбор тура, Раннее Бронирование, сайт поиска тура">
    
</head>
<header id="menu">
    <div class="w-100 header-background-transparent">
        <div class="container-fluid">
            <div class="row  d-flex justify-content-around">
                <div class="col col-sm-3 my-2">
                    <a href="<? echo get_home_url() ?>">
                        <img class="img-fluid header-logo-wight  float-right"
                             src="<?php bloginfo("template_directory"); ?>/images/logo.png" alt="">
                    </a>
                </div>
                <div class="col-7 align-items-center justify-content-evenly d-none d-sm-flex">
                    <div class="d-flex align-items-center">
                        <a href="https://vk.com/ctb76" title="Мы Вконтакте" target="_blank">
                            <img class="mx-2" src="<?php bloginfo("template_directory"); ?>/images/social-logo/vk.svg"
                                 alt="">
                        </a>
                        <a href="https://www.instagram.com/ctb76.ru/"  title="Мы в Instagram" target="_blank">
                            <img class="mx-2"
                                 src="<?php bloginfo("template_directory"); ?>/images/social-logo/instagram.svg" alt="">
                        </a>
                        <a href="https://www.facebook.com/groups/584779218582089/" title="Мы в Facebook" target="_blank">
                            <img class="mx-2"
                                 src="<?php bloginfo("template_directory"); ?>/images/social-logo/facebook.svg" alt="">
                        </a>
                        <a href="https://ok.ru/group/55405382270984" title="Мы в Одноклассники" target="_blank">
                            <img class="mx-2"
                                 src="<?php bloginfo("template_directory"); ?>/images/social-logo/odnoklassniki-logo.svg"
                                 alt="">
                        </a>

                        <a href="tel:+7-4852-94-14-50"
                           class="text-white-no_impotant text-hover-gold font-size-1_7 mx-2 d-none d-md-block"
                           role="button" title="Телефон ЦТБ">94-14-50</a>
                    </div>
                    <button class="text-white btn button-red-style d-none eModal-18 d-lg-block">Подобрать тур</button>
                </div>
                <div class="col col-sm-2 d-flex justify-content-center align-items-center">
                    <div class="d-none d-sm-flex">
                        <a href="http://ctb76.u-on.ru">
                            <div class="lk-box-img"></div>
                        </a>
                    </div>
                    <a id="trigger-overlay"  class="ml-3">
                        <span class="text-white-no_impotant text-hover-gold hidden-1010">Меню</span>
                        <img src="<?php bloginfo("template_directory"); ?>/images/burger-old.png" alt="">
                    </a>
                    <!-- open/close -->
                    <div class="overlay overlay-slidedown">

                        <button type="button" class="overlay-close">Close</button>
                        <a href="<? echo get_home_url() ?>" class="overlay-home">Home</a>
                        <nav><?php wp_nav_menu($Menu_header); ?></nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <img class="subscription-button eModal-20 d-none d-sm-block"
         src="<? echo get_template_directory_uri() . '/images/envelope.svg' ?>" alt="">
    <script>
        jQuery(document).ready(function () {
            window.onscroll = function () {
                var scrolled = window.pageYOffset || document.documentElement.scrollTop;
                document.getElementById('menu').style.backgroundColor = scrolled == 0 ? "#ffffff00" : "black";
            };
        });
    </script>
</header>
