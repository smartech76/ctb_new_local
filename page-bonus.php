<?php
/*
Template Name: Страница Программа привилегий
*/
get_header('page');
?>
<div class="w-auto banner-vip-background">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="title-banner-block d-flex justify-content-center flex-column">
                    <h1 class="text-white font-weight-bold-title UniSans-Heavy font-size-5_5 font-size-5_5-sm">ПРОГРАММА ПРИВИЛЕГИЙ "ПЯТЬ ЗВЕЗД"</h1>
                    <span class="text-gold my-4 font-size-1_2 UniSans-Heavy">БОЛЬШЕ ПУТЕШЕСТВИЙ - БОЛЬШЕ ПРИВИЛЕГИЙ</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col d-flex align-items-center py-5 flex-column">
            <h2 class="UniSans-Heavy font-weight-very-bold text-brown font-size-50">КАРТА ТУРИСТА</h2>
            <span class="text-center">Наша программа привилегий включает 5 статусов, в зависимости
от количества и частоты путешествий.</span>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/card-2.png" alt="">
        </div>
    </div>
</div>
<div class="w-100 banner-bonus-two-background">
    <div class="container text-gold font-weight-very-bold">
        <div class="row">
            <div class="col d-flex justify-content-center my-5 text-center">
                <h3 class="UniSans-Heavy font-weight-very-bold font-size-50 font-size-50-md">КАК ПОТРАТИТЬ БОНУСЫ?</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-3 d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/rger.png" alt="">
                </div>
                <span class="my-5 UniSans-Heavy">СУВЕНИРЫ</span>
            </div>
            <div class="col-12 col-sm-3 d-flex d-flex flex-column align-items-center text-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/ewetpng.png" alt="">
                </div>
                <span class="my-5 UniSans-Heavy">СКИДКИ НА ТУР</span>
            </div>
            <div class="col-12 col-sm-3 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/12132t.png" alt="">
                </div>
                <span class="my-5 UniSans-Heavy">ПОДАРОЧНЫЕ <br> СЕРТИФИКАТЫ</span>
            </div>
            <div class="col-12 col-sm-3 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/gewe.png" alt="">
                </div>
                <span class="my-5 UniSans-Heavy">ТРАНСФЕРЫ</span>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/sgsrgh.png" alt="">
                </div>
                <span class="my-5 UniSans-Heavy">ПОДАРКИ ОТ ПАРТНЕРОВ</span>
            </div>
            <div class="col-12 col-sm-4 d-flex d-flex flex-column align-items-center text-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/drhedrh.png" alt="">
                </div>
                <span class="my-5 UniSans-Heavy">ЗАКРЫТЫЕ <br> МЕРОПРИЯТИЯ <br> ДЛЯ КЛИЕНТОВ</span>
            </div>
            <div class="col-12 col-sm-4 d-flex d-flex flex-column align-items-center">
                <div>
                    <img class="img-fluid" src="<?php bloginfo("template_directory");?>/images/rgeh.png" alt="">
                </div>
                <span class="my-5 UniSans-Heavy">СТРАХОВАНИЕ</span>
            </div>
        </div>
        <div class="row">
            <div class="col d-flex flex-column align-items-center">
                <button class="btn button-red-style text-white eModal-4">ПОЛУЧИТЬ КАРТУ</button>
                <a href="https://ctb76.ru/wp-content/uploads/2018/04/PP_na_sayt.pdf" target="_blank" class="btn button-red-style text-white my-3">ПОДРОБНЕЕ О ПРОГРАММЕ</a>
            </div>
        </div>
    </div>
</div>
<?php

get_footer('page');
?>
